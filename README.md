# README #

This is the implementation of a simple stereo visual odometry algorithm in a ROS by using the computer vision library OpenCV. This ROS package expects rectified images (e.g. from imgProc) and publishes Pose data.

### What is this repository for? ###

This ROS package contains a stereo visual odometry algorithm. Feature matching is done with ORB. The algorithm is oriented on the following paper:
"Efficient Visual Odometry Estimation using Stereo Camera"
by Wei Mou, Han Wang, Gerald Seet
